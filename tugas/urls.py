from django.urls import path
from .views import *

app_name = "tugas"
urlpatterns = [
    path('',home,name='home'),
    path('about/',about,name='about'),
    path('more/',more,name='more'),
]
